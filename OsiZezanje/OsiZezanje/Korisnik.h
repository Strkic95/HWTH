#pragma once
#include "standard_headers.h"
using std::string;
class Korisnik
{
private:
protected:
	string ime;
	struct datumRodjenja
	{
		int dd;
		int mm;
		int gggg;
		std::string getDateAsString();
	};
	double plata;
	const string username;
	string password;
public:
	Korisnik();
	~Korisnik();
};

