#include "MainMenu.h"
using std::cout;
using std::cin;
using std::endl;

MainMenu* MainMenu::instance = nullptr;

void MainMenu::start() {
	while (active) {
		system("cls");
		drawLogo();
		izbor();
	}
}

void MainMenu::izbor()
{
	char input;
	cout << "Dobrodosli u glavni meni!" << endl;
	cout << "Izaberite opciju: " << endl;
	cout << "   Prijava [P]" << endl;
	cout << "   IZLAZ [0]" << endl;
	cin >> input;
	switch (input)
	{
	case('P'):
	{
		login();
		active = false;
		break;
	}
	case('0'):
	{
		setActive(false);
		break;
	}
	}
	return;
}