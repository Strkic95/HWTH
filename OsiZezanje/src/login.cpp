#include "login.h"

using std::string;
using std::cout;
using std::endl;

bool login() {
	string pass = "";
	char ch;
	cout << "Enter pass\n";
	string passStored = "OSI2016";

	while (pass != passStored)
	{
		ch = _getch();
		while (ch != 13) {
			pass.push_back(ch);
			cout << '*';
			ch = _getch();
		}
		if (pass != passStored)
		{
			pass = "";
			cout << endl << "Lozinka netacna, pokusajte ponovo!" << endl;
		}
	}
	cout << endl;
	cout << "Uspjesna prijava!" << endl;
	return true;
}