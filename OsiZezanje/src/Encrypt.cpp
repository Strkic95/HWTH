﻿#include "VirtualAES.h"
#include <iostream>
#include <windows.h>
#include <time.h>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
	/* All char arrays are null terminated here...*/
	char cipherhex[256];
	char decryptedhex[128];
	uchar dechex[128];

	uchar szkey[KEY_128] = "very strong key";
	uchar szPlaintext[128] = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa";
	uchar szCiphertext[128];
	uchar szDecryptedtext[128];

	aes_ctx_t *ctx;
	u64 nonce;

	virtualAES::initialize();
	ctx = virtualAES::allocatectx(szkey, sizeof(szkey));
	virtualAES::rand_nonce(&nonce);

	/*encrypt*/
	virtualAES::encrypt_ctr(ctx, szPlaintext, szCiphertext, sizeof(szPlaintext), nonce);

	cout << "cipherdata in ansi:\n" << szCiphertext << "\n\n";
	virtualAES::strtohex(szCiphertext, cipherhex, 128);
	cout << "cipherdata in hex:\n" << cipherhex << "\n\n";
	/*decrypt*/

	virtualAES::hextostr("340A0A94EA7BAA70A3672E5D47C2529AA0D"
		"7835CD1A5FDF45CFD3D0945F705AC7FBEC8EF4FBB782B710C4BE4"
		"C3EF8E4F418A3D213D81344192E4DE32907DF3FDCF6B87756BE2D"
		"55909E37ABD35C7A634DD0424905F648E74BC7E258E8029D1F66F"
		"48529D263321B7644CF4AD818467A0AA80FC4700B3F3360A284E4"
		"ED4A7ABE8", dechex, 128);

	virtualAES::decrypt_ctr(ctx, dechex, szDecryptedtext, sizeof(dechex), nonce);	
	cout << "decrypted data in ansi:\n" << szDecryptedtext << "\n\n";
	virtualAES::strtohex(szDecryptedtext, decryptedhex, 128);
	cout << "decrypted data in hex:\n" << decryptedhex << endl;

	getchar();
	return EXIT_SUCCESS;
}