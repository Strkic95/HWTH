#pragma once
#include "standard_headers.h"
#include "Vrijeme.h"
class Vozilo
{
	std::string tablice;
	char kategorija;
public:
	Vozilo(std::string __tablice, char __kategorija) : tablice(__tablice), kategorija(__kategorija) {};
	std::string generateInputCSV();
	Vozilo();
	~Vozilo();
};

