#pragma once
#include "login.h"
#include "Draw.h"
#include "Vrijeme.h"
//11.12.2016. realizacija main menu-a kao klase

class MainMenu { 
public:
	static MainMenu* getInstance() {
		if (instance == nullptr)
			instance = new MainMenu();
		return instance;
	}

	void start();
	void izbor();

	void setActive(bool active) { this->active = active; }
	bool isActive() const { return this->active; }
private:
	MainMenu() = default;
	MainMenu(MainMenu&) = delete;
	MainMenu(MainMenu&&) = delete;
	MainMenu& operator=(MainMenu&) = delete;
	MainMenu& operator=(MainMenu&&) = delete;

	static MainMenu* instance;

	bool active = true;
};
